$(document).ready(function () {
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll > 70) {
            $("#nav").addClass("colored");
        } else if (scroll < 70) {
            $("#nav").removeClass("colored");
        }
    });
});
